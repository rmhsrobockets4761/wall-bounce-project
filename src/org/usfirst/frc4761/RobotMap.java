package org.usfirst.frc4761;

import edu.wpi.first.wpilibj.Accelerometer;
import edu.wpi.first.wpilibj.AnalogChannel;
import edu.wpi.first.wpilibj.Victor;

/**
 * The RobotMap is a mapping from the ports sensors and actuators are wired into
 * to a variable name. This provides flexibility changing wiring, makes checking
 * the wiring easier and significantly reduces the number of magic numbers
 * floating around.
 */
public class RobotMap {
    public static final int leftPort = 6;
    public static final int rightPort = 7;
    public static final Victor leftMotor = new Victor(leftPort);
    public static final Victor rightMotor = new Victor(rightPort);

    public static final int distancePort = 2;
    public static final AnalogChannel distanceSensor = new AnalogChannel(distancePort);
    
    public static final int accelerometerPort = 1;
    public static final Accelerometer accelerometerSensor = new Accelerometer(accelerometerPort);
}
