package org.usfirst.frc4761.commandgroups;

/*
 * This command runs when autonomous mode is entered
*/

import edu.wpi.first.wpilibj.command.CommandGroup;
import org.usfirst.frc4761.commands.CheckAcceleration;
import org.usfirst.frc4761.commands.CheckDistance;
import org.usfirst.frc4761.commands.Decelerate;
import org.usfirst.frc4761.commands.GoForward;

/**
 *
 * @author jakekinsella
 */
public class AutonomousCommand extends CommandGroup {
    
    public AutonomousCommand() {
        addSequential(new CheckAcceleration());
        addSequential(new GoForward());
        addParallel(new CheckDistance());
        addSequential(new Decelerate(0));
        addSequential(new AvoidWall());
    }
}
