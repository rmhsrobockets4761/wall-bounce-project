/*
 * This command attempts to avoid the wall
*/

package org.usfirst.frc4761.commandgroups;

import edu.wpi.first.wpilibj.command.CommandGroup;
import org.usfirst.frc4761.commands.CheckDistance;
import org.usfirst.frc4761.commands.Decelerate;
import org.usfirst.frc4761.commands.GoBackwards;
import org.usfirst.frc4761.commands.TurnLeft;

/**
 *
 * @author jakekinsella
 */
public class AvoidWall extends CommandGroup {
    
    public AvoidWall() {
        addSequential(new GoBackwards());
        addSequential(new Decelerate(0));
        addSequential(new TurnLeft());
        addParallel(new CheckDistance());
        addSequential(new Decelerate(1));
    }
}
