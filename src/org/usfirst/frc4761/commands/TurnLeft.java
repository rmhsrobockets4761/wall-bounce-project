/*
 * This command turns the robot to the left
*/

package org.usfirst.frc4761.commands;

/**
 *
 * @author jakekinsella
 */
public class TurnLeft extends CommandBase {
    
    public TurnLeft() {
        // Use requires() here to declare subsystem dependencies
        // eg. requires(chassis);
        requires(leftMotor);
        requires(rightMotor);
    }

    // Called just before this Command runs the first time
    protected void initialize() {
    }

    // Called repeatedly when this Command is scheduled to run
    protected void execute() {
        setTimeout(1);
        leftMotor.goLeft(0.3);
        rightMotor.goRight(0.3);
    }

    // Make this return true when this Command no longer needs to run execute()
    protected boolean isFinished() {
        return isTimedOut();
    }

    // Called once after isFinished returns true
    protected void end() {
        leftMotor.stop();
    }

    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    protected void interrupted() {
        end();
    }
}
