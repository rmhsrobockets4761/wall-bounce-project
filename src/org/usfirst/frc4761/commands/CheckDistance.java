/*
 * This command checks the distance between the distance
 * sensor and the object that the sensor is pointed at
*/

package org.usfirst.frc4761.commands;

/**
 *
 * @author jakekinsella
 */
public class CheckDistance extends CommandBase {
    private boolean finished = false;
    
    public CheckDistance() {
        requires(distanceSensor);
    }

    // Called just before this Command runs the first time
    protected void initialize() {
    }

    // Called repeatedly when this Command is scheduled to run
    protected void execute() {
        // Check to see if the nearest object is less than 1000mm away
        if (distanceSensor.getDistance() <= 1000) {
            finished = true;
        }
    }

    // Make this return true when this Command no longer needs to run execute()
    protected boolean isFinished() {
        return finished;
    }

    // Called once after isFinished returns true
    protected void end() {
    }

    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    protected void interrupted() {
    }
}
