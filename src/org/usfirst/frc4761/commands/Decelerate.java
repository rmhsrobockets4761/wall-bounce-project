/*
 * This command slows the motors to zero
*/

package org.usfirst.frc4761.commands;

/**
 *
 * @author jakekinsella
 */
public class Decelerate extends CommandBase {
    private int waitTime = 0;
    
    public Decelerate(int seconds) {
        this.waitTime = seconds;
        
        requires(leftMotor);
        requires(rightMotor);
    }

    // Called just before this Command runs the first time
    protected void initialize() {
    }

    // Called repeatedly when this Command is scheduled to run
    protected void execute() {
        setTimeout(waitTime);
        
        while (!isTimedOut()) {} // Wait for time to be up
        
        double leftSpeed = leftMotor.getSpeed();
        double rightSpeed = rightMotor.getSpeed(); 
        double target = 0;
        double increaseLeft = .1;
        double increaseRight = .1;
        
        int i = 0;
        
        boolean isLeftNegative = leftMotor.isNegative();
        boolean isRightNegative = rightMotor.isNegative();
        
        if (Math.abs(leftSpeed) <= Math.abs(rightSpeed)) {
            target = Math.abs(rightSpeed);
        } else {
            target = Math.abs(leftSpeed);
        }
        
        while (i < target) {            
            // Check to see if the speed has gone past zero
            if (isLeftNegative != leftMotor.isNegative()) {
                leftMotor.stop();
                increaseLeft = 0;
            }
            
            if (isRightNegative != rightMotor.isNegative()) {
                rightMotor.stop();
                increaseRight = 0;
            }
            
            if (leftMotor.isNegative()) {
                leftMotor.setSpeed(leftMotor.getSpeed() + increaseLeft);
            } else {
                leftMotor.setSpeed(leftMotor.getSpeed() - increaseLeft);
            }
            
            if (rightMotor.isNegative()) {
                rightMotor.setSpeed(rightMotor.getSpeed() + increaseRight);
            } else {
                rightMotor.setSpeed(rightMotor.getSpeed() - increaseRight);
            }
        }
    }

    // Make this return true when this Command no longer needs to run execute()
    protected boolean isFinished() {
        return false;
    }

    // Called once after isFinished returns true
    protected void end() {
    }

    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    protected void interrupted() {
    }
}
