/*
 * This command drives the robot backwards
*/

package org.usfirst.frc4761.commands;

/**
 *
 * @author jakekinsella
 */
public class GoBackwards extends CommandBase {
    
    public GoBackwards() {
        requires(leftMotor);
        requires(rightMotor);
    }

    // Called just before this Command runs the first time
    protected void initialize() {
    }

    // Called repeatedly when this Command is scheduled to run
    protected void execute() {
        leftMotor.goBackwards(0.3);
        rightMotor.goBackwards(0.3);
    }

    // Make this return true when this Command no longer needs to run execute()
    protected boolean isFinished() {
        return true;
    }

    // Called once after isFinished returns true
    protected void end() {
    }

    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    protected void interrupted() {
        end();
    }
}
