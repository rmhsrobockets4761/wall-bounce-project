package org.usfirst.frc4761.subsystems;

import edu.wpi.first.wpilibj.Accelerometer;
import edu.wpi.first.wpilibj.command.Subsystem;
import org.usfirst.frc4761.RobotMap;

/**
 *
 * @author jakekinsella
 */
public class AccelerometerSensor extends Subsystem {
    private Accelerometer sensor = RobotMap.accelerometerSensor;

    public void initDefaultCommand() {
    }
    
    public double getAcceleration () {
        return sensor.getAcceleration();
    }
}
