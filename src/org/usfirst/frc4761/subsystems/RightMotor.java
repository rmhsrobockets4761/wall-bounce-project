package org.usfirst.frc4761.subsystems;

import edu.wpi.first.wpilibj.Victor;
import edu.wpi.first.wpilibj.command.Subsystem;
import org.usfirst.frc4761.RobotMap;

/**
 *
 * @author jakekinsella
 */
public class RightMotor extends Subsystem  {
    Victor motor = RobotMap.rightMotor;
    
    public void initDefaultCommand() {

    }
    
    public void setSpeed (double speed) {
        motor.set(speed);
    }
    
    public void goForward (double speed) {
        motor.set(speed);
    }
    
    public void goBackwards (double speed) {
        motor.set(-speed);
    }
    
    public void goRight (double speed) {
        motor.set(speed);
    }
    
    public void goLeft (double speed) {
        motor.set(-speed);
    }
        
    public void stop () {
        motor.set(0);
    }
        
    public double getSpeed () {
        return motor.get();
    }
        
    public boolean isNegative () {
        if (motor.get() < 0) {
            return true;
        } else {
            return false;
        }
    }
}
