package org.usfirst.frc4761.subsystems;

import edu.wpi.first.wpilibj.AnalogChannel;
import edu.wpi.first.wpilibj.command.Subsystem;
import org.usfirst.frc4761.RobotMap;

/**
 *
 * @author jakekinsella
 */
public class DistanceSensor extends Subsystem {
    AnalogChannel sensor = RobotMap.distanceSensor;

    public void initDefaultCommand() {

    }
    
    private double toMilliVolts (double voltage) {
        return voltage * 1000;
    }
    
    public double getDistance () {
        return (toMilliVolts(sensor.getVoltage()) / 4.885) * 5;
    }
}
