# The Robockets Wall Bouncer Project #

Drive around autonomously, and use the distance sensor to avoid obstacles.

### How will it work? ###
* Drive in a straight line
* Detect an obstacle
* Decelerate
* Back Up
* Turn

### Default Ports Summary ###
* Left Motor: PWM 6
* Right Motor: PWM 7
* Ultrasonic Sensor: Analog Channel 2